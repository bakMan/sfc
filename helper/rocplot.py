# coding: utf-8
from sklearn.metrics import roc_curve
import csv
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
import sys

#cut values and convert
strs = sys.stdin.read().splitlines();
vals = [ (float(x.split("  ")[0]),float(x.split("  ")[1])) for x in strs]
vals = [(x,int(y)) for x,y in vals]

#unzip
scores, labels = zip(*vals)

#make roc
fpr,tpr,_ = roc_curve(labels,scores)

#plot and save
plt.plot(fpr,tpr)
plt.title("ROC curve of BP Neural Network on Adult dataset")
plt.title("Performance of BP Neural Network on Adult dataset")
plt.ylabel("True positive rate")
plt.xlabel("False positive rate")
plt.savefig("roc.svg")

#calculate auc and output roc
print(str(roc_auc_score(labels,scores)));