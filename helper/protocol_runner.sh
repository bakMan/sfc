#! /bin/bash

function strip()
{
	
	echo $1 | python3 -c 'import sys; print(sys.stdin.read().strip())'
}

#name of evaluation script
EVALUATOR_NAME=rocplot.py

#path to this file
SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

#path to evaluator
EVALUATOR=$SCRIPTPATH/$EVALUATOR_NAME

OUT_DIR=protocol_output

#two parameters required - path to bpnn file and testing protocol
if [[ ! $# -eq 4 ]]; then
	echo "Error: expecting fours parameters - path to bpnn binary, path to testing protocol, prefix of dataset and number of times to repeat process." 1>&2 
	exit 1
fi

BPNN=$1
PROTOCOL=$2
PREFIX=$3
TIMES=$4

if [[ -e $OUT_DIR ]]; then
	#statement
	rm -r $OUT_DIR
fi
mkdir $OUT_DIR

echo "" > $OUT_DIR/restults.txt

#for desired number of times
for (( i = 0; i < $TIMES; i++ )); do
	
	#train and classify
	
	while read -r line || [[ -n "$line" ]]; do

		#unpack line - settings and their label
		settings=$(echo "$line" | cut -d';' -f1)
		label=$(echo "$line" | cut -d';' -f2)

		settings=$(strip "$settings")
		label=$(strip "$label")

	    #create output dir for this setting
	    mkdir -p $OUT_DIR/$i/$label
	    OUT=$OUT_DIR/$i/$label

	    echo "[RUNNER] Training network with options '$settings' ($label)" 1>&2
	    $BPNN -t $settings "${PREFIX}_train.csv" "${PREFIX}_train_labels.csv" > $OUT/network.txt

	    echo "[RUNNER] Classifing test set" 1>&2
	    $BPNN -c ${PREFIX}_test.csv $OUT/network.txt > $OUT/result.txt
		
		#delete leading whitespace -
		sed -i 's/^ *//' $OUT/result.txt
	    
	    echo "[RUNNER] Evaluating results" 1>&2
	    paste -d' ' $OUT/result.txt ${PREFIX}_test_labels.csv > $OUT/result_labels.txt
	    AUC=$(cd $OUT && cat result_labels.txt | python3 $EVALUATOR )
	    echo "$label $AUC" >> $OUT_DIR/results.txt

	done < "$PROTOCOL"
	

done

wait