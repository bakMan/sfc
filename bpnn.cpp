/* Neural network implementation
 * File:   bpnn.cpp
 * Author: Stanislav Smatana
 *

 */
#include <stdexcept>
#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include <cassert>

#include "bpnn.hpp"
#include "matrix.hpp"

using namespace std;

/** Logistic sigmoid function
 * 
 * @param x
 * @return 
 */
double sigmoid(double x)
{
    return 1 / (1 + pow(E,-x));
}

/** Constructs neural network of given topology
 * 
 * @param vec List of layer sizes. First integer denotes dimensionality of input.
 */
bpnn::bpnn(std::vector<unsigned> vec)
{
    //Minimal size of the vector is 2 - 1) Input size 2) One layer
    if(vec.size() < 2){
        throw std::runtime_error("Neural network hast to have at least one layer.");
    }
    
    //Number of neurons is given by number in vector, number of weights
    //is given by number of neurons in preceeding layer
    for(int i=1;i < vec.size();i++)
    {
        if(vec.at(i-1) == 0 || vec.at(i) == 0)
            throw std::runtime_error("Layer or input can't have zero items.");
        
        //neurons are columns, weights are in rows
        t_shape s = {vec.at(i),vec.at(i-1)};
        layers.push_back( Matrix(s));
        
        //one row for bias
        s.cols = 1;
        biases.push_back(Matrix(s));
        
        t_shape out_shape = {vec.at(i),1};
        outputs.push_back(Matrix(out_shape));
    }
 
    //Make random number generator 
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-0.5, 0.5);
    
    //Now initialize by random values
    for(Matrix &x : layers)
        x.map([&](double a){return dis(gen);});
    
    for(Matrix &x : biases)
    {
        x.map([&](double a){return dis(gen);});
    }
        
}

//Convience initializer from init list
//bpnn::bpnn(std::initializer_list<std::initializer_list<std::initializer_list<double> > > init)
//{
//    if(init.size() < 1)
//        throw std::runtime_error("Neural network has to have at least one layer");
//    
//    for(auto x : init)
//    {
//        //each should be init of matrix
//        layers.push_back(Matrix(x));
//        
//        t_shape s = {layers.rbegin()->shape.cols,1};
//    }
//}

/** Outputs neural network description to the stdin */
void bpnn::printNN()
{
    auto bit = biases.begin();
    for(Matrix &x : layers)
    {
        x.printMat();
        std::cout << std::endl;
        
        bit->printMat();
        std::cout << std::endl;
        bit++;
    }
}

/** Classifies given vector
 *  
 * @param vector Vector to classify
 * @return Output vector of network
 */
Matrix bpnn::classify(Matrix vector){
    
    auto bit = biases.begin();
    auto oit = outputs.begin();
    for(Matrix &x : layers)
    {
        MatrixView &input =  oit == outputs.begin() ? vector : *(oit - 1);
        //Calculate LBF
        dot(x,input,*oit);
        
        //add bias
        oit->operator +=(*bit);
        
        //Apply sigmoid
        oit->map(sigmoid);
        
        //move bias and output iterator
        bit++;
        oit++;
    }
    
    return *outputs.rbegin();
}


/** Trains neural network using backpropagation
 * 
 * @param input      Matrix of training samples
 * @param expected   Matrix of expected outputs
 * @param rate       Learning rate
 * @param eps        Minimal error below which the learning ends
 * @param max_iters  Maximal number of algorithm iterations
 */
void bpnn::train(Matrix &input,Matrix &expected,double rate,double eps,int max_iters)
{
    if(expected.shape.cols  != layers.rbegin()->shape.rows)
        throw std::runtime_error("Last layer has to have same number of neurons as is columns in expected vectors.");
    
    if(input.shape.cols  != layers.begin()->shape.cols)
        throw std::runtime_error("Size of input data does not match input size of network.");
    
    for (auto &x : layers)
        assert(std::all_of(x.data,x.data + x.shape.cols*x.shape.rows,[](double a){return a>=-0.5 && a <= 0.5;}));
        
    for (auto &x : biases)
        assert(std::all_of(x.data,x.data + x.shape.cols*x.shape.rows,[](double a){return a>=-0.5 && a <= 0.5;}));
        
    int i=0;
    double GLErr = -100000;
    double GLErr_old = 0;
    vector<Matrix> deltas(layers.size());
    
    //Create tmp matrix for results
    t_shape s={input.shape.rows,expected.shape.cols};

    do
    {
        GLErr_old = GLErr;
        GLErr = 0.0;
        //Evaluate each sample in the training set
        for(int r=0;r < input.shape.rows;r++)
        {
            
            //get row and its corresponding expected value
            Matrix row = input.getRow(r);
            Matrix transrow = row.transpose();
            Matrix exp_row = expected.getRow(r).transpose();
            
            //classify
            classify(transrow);
            
            for(auto &x:outputs)
                assert(std::all_of(x.data,x.data + x.shape.cols*x.shape.rows,[](double a){return a>=0 && a <= 1;}));
            
            Matrix &y = *outputs.rbegin();
            
            //calculate error
            Matrix error_vec = exp_row - y;
            double error = error_vec.ssum();
            GLErr += 0.5*error;
            
            //calculate delta of output layer
            //-1 !!!! derivative has opposite sign to error
            //error_vec.map([](double a){return -a;});
            Matrix delta = elementMultiply(error_vec,y);
            applyElementwise(delta,y,delta,[](double a,double b){return a*(1-b);});
            
            //backpropagate
            auto oit = outputs.rbegin();
            oit++; //last output is not input to the outmost layer -> increment
            
            auto bit = biases.rbegin();
            auto lit = layers.rbegin();
                        
            //For because first layer input is not in outputs - it is the input
            //of the whole network
            for(int i=0;i < outputs.size() ;i++)
            {
                //Either output, or input of the whole network
                Matrix &output = oit != outputs.rend() ? *oit : transrow;
                    
                //calculate delta
                Matrix newdelta = dot(TransposedView(*lit),delta);
                elementMultiply(newdelta,output,newdelta);
                applyElementwise(newdelta,output,newdelta,[](double a,double b){return a*(1-b);});
                
                //update weight
                Matrix deltamat = Matrix(lit->shape);
                dot(delta,TransposedView(output),deltamat);
                deltamat.map([rate](double a){return rate*a;});
                lit->operator +=(deltamat);
                
                //update bias
                applyElementwise(*bit,delta,*bit,[rate](double a,double b){return a + rate*b;});
                //bit->operator +=();
                
                delta = newdelta;
                
                //advance iterators
                if(oit != outputs.rend())
                    oit++;
                
                lit++;
                bit++;
            }
            
       
        }
        
        i++;
        std::cerr << "[EPOCH " << i << "]" <<  "Error: " << GLErr << endl;
    }while(( GLErr > eps) && (max_iters==-1 || i < max_iters));    
    
}

/** Deserialize network from given stream
 * 
 * @param stream Stream to read
 */
void bpnn::read(std::istream &stream){
    layers.clear();
    Matrix tmp;
    
    int n=0;
    while(tmp.read(stream))
    {
        //layer first - than bias
        if(n%2 == 0)
        {
            layers.push_back(tmp);
        }
        else
        {
            biases.push_back(tmp);
            outputs.push_back(Matrix(tmp.shape));
        }
        n++;
    }
}