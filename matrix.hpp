/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   matrix.hpp
 * Author: bakman
 *
 * Created on October 19, 2016, 11:05 AM
 */

#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <functional>
#include <initializer_list>
#include <stdexcept>
#include <istream>

struct shape{
    unsigned rows;
    unsigned cols;
};

typedef struct shape t_shape;


class MatrixView
{
public:
    t_shape shape;
    virtual double get(unsigned r,unsigned c) const = 0;
};

class Matrix : public MatrixView {
    
    
    private:
    bool owner = true;
     
    //Shape is public but constant and set in constructor
    public:
    double *data = nullptr;
    
    Matrix();
    Matrix(t_shape s);
    Matrix(t_shape s,double *data);
    Matrix(std::initializer_list< std::initializer_list<double> > init);
    Matrix(std::initializer_list<double> init);
    ~Matrix();
    Matrix(const Matrix &m);
    Matrix(Matrix &&m);
    Matrix& operator=(Matrix other);
    
    Matrix operator*( const MatrixView &other)const;
    
    Matrix operator-();
    Matrix operator-(const Matrix &other);
    Matrix operator+(const Matrix &other);
    void minus(const Matrix &other, Matrix &out)const;
    
    Matrix& operator+=(MatrixView &other);
    
    friend void swap(Matrix& first, Matrix& second);
    
    double get(unsigned r,unsigned c) const;
    void set(unsigned r,unsigned c, double val);
    void set(unsigned i,double val);
    void reshape(t_shape n_shape);
    
    void setRow(Matrix &in,unsigned r);
    void getRowNocpy(unsigned r,Matrix &out) const;
    Matrix getRow(unsigned r);
 
    Matrix &map(std::function<double(double)>);
    
    /** Maps function onto elemnts but returns new matrix
     * 
     * @param Function of form double(double)
     * @return New matrix with transformed values
     */
    Matrix map_new(std::function<double(double)>);
    double sum();
    double ssum();
    double max();
    Matrix transpose();
    
    friend void swap(Matrix& first, Matrix& second);
    friend Matrix applyElementwise(const Matrix &a,const Matrix &b,std::function<double(double,double)> fun);
    friend void applyElementwise(const Matrix &a,const Matrix &b,Matrix &out,std::function<double(double,double)> fun);
    friend Matrix elementMultiply(const Matrix &a,const Matrix &b);
    friend void elementMultiply(const Matrix &a,const Matrix &b,Matrix &out);
    
    void printMat();
    
    /** Function appends 1.0 to the end of column vector.
     *  This is done without memory reallocation.
     *  It is used for bias calculation in NN classification.
     * 
     */
    void implicitONE();
    void popBack();
    
    Matrix cutAtColumn(unsigned col);
    
    bool read(std::istream &stream);
    
     explicit operator double();
        
        
};

class TransposedView : public MatrixView
{
private:
    Matrix &mat;
    
public:
    TransposedView(Matrix &m):mat(m)
    {
        //shape is transposed
        shape = {mat.shape.cols,mat.shape.rows};
    }
    
    double get(unsigned r,unsigned c) const{
        //return transposed version
        return mat.get(c,r);
    }
    
};

void dot(const MatrixView &a,const MatrixView &other, Matrix &out);
Matrix dot(const MatrixView &a,const MatrixView &other);
void implicitDot(const MatrixView &a,const Matrix &other,Matrix &out);
#endif /* MATRIX_HPP */

